imsg ()
{
	echo "I: $*"
}

emsg ()
{
	echo "E: $*"
}

wmsg ()
{
	echo "W: $*"
}

emsgx ()
{
	RET=$1 && shift
	emsg $*
	exit $RET
}
